<?php
/**
 * @file default_settings.inc
 *
 * Set defaults for dev environments or just for examples.  These
 * are variables that change based on environment (eg URL).
 *
 * This file should be used sparingly.
 */
