; Modules
; --------

api = 2
core = 7.x

;projects[chatroom][subdir] = contrib
;projects[chatroom][type] = module
;projects[chatroom][version] = 1.0-beta1

projects[addressfield][subdir] = contrib
projects[addressfield][type] = module
projects[addressfield][version] = 1.x-dev

projects[advanced_help][subdir] = contrib
projects[advanced_help][type] = module
projects[advanced_help][version] = 1.0

projects[backup_migrate][subdir] = contrib
projects[backup_migrate][type] = module
projects[backup_migrate][version] = 2.2

projects[colorbox][subdir] = contrib
projects[colorbox][type] = module
projects[colorbox][version] = 1.2

projects[commerce][subdir] = contrib
projects[commerce][type] = module
projects[commerce][version] = 1.x-dev

projects[commerce_addressbook][subdir] = contrib
projects[commerce_addressbook][type] = module
projects[commerce_addressbook][version] = 1.x-dev

projects[commerce_authnet][subdir] = contrib
projects[commerce_authnet][type] = module
projects[commerce_authnet][version] = 1.x-dev

projects[commerce_bpc][subdir] = contrib
projects[commerce_bpc][type] = module
projects[commerce_bpc][version] = 1.0-rc5

projects[commerce_cardonfile][subdir] = contrib
projects[commerce_cardonfile][type] = module
projects[commerce_cardonfile][version] = 1.x-dev

projects[commerce_features][subdir] = contrib
projects[commerce_features][type] = module
projects[commerce_features][version] = 1.x-dev

projects[commerce_order_reference][subdir] = contrib
projects[commerce_order_reference][type] = module
projects[commerce_order_reference][version] = 1.0-alpha1

projects[commerce_price_savings_formatter][subdir] = contrib
projects[commerce_price_savings_formatter][type] = module
projects[commerce_price_savings_formatter][version] = 1.x-dev

projects[commerce_recurring][subdir] = contrib
projects[commerce_recurring][type] = module
projects[commerce_recurring][version] = 1.x-dev

projects[commerce_shipping][subdir] = contrib
projects[commerce_shipping][type] = module
projects[commerce_shipping][version] = 2.x-dev

projects[commerce_wishlist][subdir] = contrib
projects[commerce_wishlist][type] = module
projects[commerce_wishlist][version] = 1.x-dev

;projects[context][subdir] = contrib
;projects[context][type] = module
;projects[context][version] = 3.0-beta2

projects[ctools][subdir] = contrib
projects[ctools][type] = module
projects[ctools][version] = 1.x-dev

projects[custom_breadcrumbs][subdir] = contrib
projects[custom_breadcrumbs][type] = module
projects[custom_breadcrumbs][version] = 1.0-alpha1

projects[date][subdir] = contrib
projects[date][type] = module
projects[date][version] = 2.x-dev

projects[ds][subdir] = contrib
projects[ds][type] = module
projects[ds][version] = 1.4

projects[email][subdir] = contrib
projects[email][type] = module
projects[email][version] = 1.0

projects[entity][subdir] = contrib
projects[entity][type] = module
projects[entity][version] = 1.0-rc1

;projects[fbconnect][subdir] = fbconnect
;projects[fbconnect][type] = module
;projects[fbconnect][version] = 2.x-dev

projects[features][subdir] = contrib
projects[features][type] = module
projects[features][version] = 1.0-beta6

projects[feeds][subdir] = contrib
projects[feeds][type] = module
projects[feeds][version] = 2.x-dev

projects[field_group][subdir] = contrib
projects[field_group][type] = module
projects[field_group][version] = 2.x-dev

projects[fivestar][subdir] = contrib
projects[fivestar][type] = module
projects[fivestar][version] = 2.x-dev

projects[flag][subdir] = contrib
projects[flag][type] = module
projects[flag][version] = 2.0-beta6

projects[gmap][subdir] = contrib
projects[gmap][type] = module
projects[gmap][version] = 1.x-dev

projects[google_analytics][subdir] = contrib
projects[google_analytics][type] = module
projects[google_analytics][version] = 1.x-dev

projects[imagezoom][subdir] = contrib
projects[imagezoom][type] = module
projects[imagezoom][version] = 1.1

projects[interval][subdir] = contrib
projects[interval][type] = module
projects[interval][version] = 1.0-alpha5

projects[job_scheduler][subdir] = contrib
projects[job_scheduler][type] = module
projects[job_scheduler][version] = 2.x-dev

projects[libraries][subdir] = contrib
projects[libraries][type] = module
projects[libraries][version] = 2.x-dev

projects[link][subdir] = contrib
projects[link][type] = module
projects[link][version] = 1.0

projects[location][subdir] = contrib
projects[location][type] = module
projects[location][version] = 3.x-dev

projects[location_feeds][subdir] = contrib
projects[location_feeds][type] = module
projects[location_feeds][version] = 1.4

projects[media][subdir] = contrib
projects[media][type] = module
projects[media][version] = 1.0-rc3

;projects[mediaelement][subdir] = contrib
;projects[mediaelement][type] = module
;projects[mediaelement][version] = "1.1"

projects[navigation404][subdir] = contrib
projects[navigation404][type] = module
projects[navigation404][version] = 1.0

projects[nodeconnect][subdir] = contrib
projects[nodeconnect][type] = module
projects[nodeconnect][version] = 1.x-dev

projects[nodequeue][subdir] = contrib
projects[nodequeue][type] = module
projects[nodequeue][version] = 2.x-dev

projects[panels][subdir] = contrib
projects[panels][type] = module
projects[panels][version] = 3.0

projects[pathauto][subdir] = contrib
projects[pathauto][type] = module
projects[pathauto][version] = 1.0

;projects[plupload][subdir] = contrib
;projects[plupload][type] = module
;projects[plupload][version] = "1.x-dev"

projects[radioactivity][subdir] = contrib
projects[radioactivity][type] = module
projects[radioactivity][version] = 2.1

projects[rolereference][subdir] = contrib
projects[rolereference][type] = module
projects[rolereference][version] = 1.0
projects[rolereference][patch][] = http://drupal.org/files/rolereference-7.x-1.x-missing-property-type-1372398-1.patch

projects[references][subdir] = contrib
projects[references][type] = module
projects[references][version] = 2.0

projects[rules][subdir] = contrib
projects[rules][type] = module
projects[rules][version]= 2.x-dev

projects[scheduler][subdir] = contrib
projects[scheduler][type] = module
projects[scheduler][version] = 1.0

projects[strongarm][subdir] = contrib
projects[strongarm][type] = module
projects[strongarm][version] = 2.0-beta5

;projects[subpathauto][subdir] = contrib
;projects[subpathauto][type] = module
;projects[subpathauto][version] = 1.1

projects[styles][subdir] = contrib
projects[styles][type] = module
projects[styles][version] = "2.x-dev"

projects[token][subdir] = contrib
projects[token][type] = module
projects[token][version] = 1.x-dev

projects[video][subdir] = contrib
projects[video][type] = module
projects[video][version] = 2.4

projects[views][subdir] = contrib
projects[views][type] = module
projects[views][version] = 3.1

projects[views_slideshow][subdir] = contrib
projects[views_slideshow][type] = module
projects[views_slideshow][version] = 3.0

projects[votingapi][subdir] = contrib
projects[votingapi][type] = module
projects[votingapi][version] = 2.x-dev

;projects[webform][subdir] = contrib
;projects[webform][type] = module
;projects[webform][version] = 3.13

projects[wysiwyg][subdir] = contrib
projects[wysiwyg][type] = module
projects[wysiwyg][version] = 2.1

; Development =================================================================

projects[devel][subdir] = contrib
projects[devel][type] = module
projects[devel][version] = 1.x-dev

; Custom and Downloadable =====================================================
