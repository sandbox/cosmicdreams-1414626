<?php
/**
 * @file owc.profile
 * This file is analogous to a modules .module file and allows the profile
 * do most things a module does - use it sparingly.
 */

/**
 * Implements hook_boot().
 */
 /*
function owc_boot() {
  // Make some guesses about default configuration if this isn't production.
  if (!isset($_SERVER['PROD'])) {
    // Expose $base_url to conf file.
    global $base_url;
    $conf = array();
    // Include a configuration file for setting variables.
    require_once dirname(__FILE__) . '/default_settings.inc';
    // Set as defaults, not overrides.
    $GLOBALS['conf'] = array_merge($conf, $GLOBALS['conf']);
  }
}
*/
/**
 * Implements hook_form_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function owc_form_install_configure_form_alter(&$form, $form_state) {
  // Set a default name for the dev site.
  $form['site_information']['site_name']['#default_value'] = t('The Kim Komando Show');

  // Set a default email address for the dev site.
  $form['site_information']['site_mail']['#default_value'] = 'kimkomando@nerdery.com';

  // Set a default country so I can benefit from it on Address Fields.
  $form['server_settings']['site_default_country']['#default_value'] = 'US';

  // Turn off update status notifications.
  $form['update_notifications']['update_status_module']['#default_value'] = array();

}
