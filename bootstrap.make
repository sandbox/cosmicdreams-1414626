;; ### Includes

;; An array of makefiles to include. Each include may be a local relative path
;; to the includer makefile directory or a direct URL to the makefile. Includes
;; are appended in order with the source makefile appended last, allowing latter
;; makefiles to override the keys/values of former makefiles.

; List of make files ordered by execution precedence
includes[distro] = "distro.make"
includes[epilogue] = "epilogue.make"

;; Overrides
;; 
;; Any last minute overrides go below this line
