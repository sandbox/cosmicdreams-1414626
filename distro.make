; Use this file to build a full distro including Drupal core (with patches) and
; the install profile using the following command:
;
;     $ drush make distro.make www --working-copy
api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = 7

; Specify the themes to retrieve, and place into the sites/all/themes directory
; Add theme to build (sites/all/themes) when one becomes available
projects[komando][type] = theme
projects[komando][download][type] = svn
projects[komando][download][url] = https://svn.sierrabravo.net/TBANY.kimkommando/trunk/code/themes/komando

; Specify the installation profile to use.
; Add Kim Kommando Profile to the full Drupal distro build
projects[kim][type] = profile
projects[kim][download][type] = svn
projects[kim][download][url] = https://svn.sierrabravo.net/TBANY.kimkommando/trunk/code/profile

; CKEditor's drupal module 
; Use this for best integration of WYSIWYG text editor
projects[ckeditor][type] = module
projects[ckeditor][download][type] = get
projects[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz

; Libraries
; ---------
libraries[colorbox][download][type] = git
libraries[colorbox][download][url] = https://github.com/jackmoore/colorbox.git
libraries[colorbox][destination] = libraries

libraries[Jcrop][download][type] = git
libraries[Jcrop][download][url] = https://github.com/tapmodo/Jcrop.git
libraries[Jcrop][destination] = libraries

libraries[json2][download][type] = git
libraries[json2][download][url] = https://github.com/douglascrockford/JSON-js.git
libraries[json2][destination] = libraries

libraries[jquery.cycle][download][type] = get
libraries[jquery.cycle][download][url] = http://ajax.aspnetcdn.com/ajax/jquery.cycle/2.99/jquery.cycle.all.min.js
libraries[jquery.cycle][destination] = libraries

; Plupload library
;libraries[plupload][download][type] = "get"
;libraries[plupload][download][url] = "https://github.com/downloads/moxiecode/plupload/plupload_1_5b.zip"
;libraries[plupload][directory_name] = "plupload"
;libraries[plupload][destination] = "libraries"

; MediaElement library
;libraries[mediaelement][download][type] = "get"
;libraries[mediaelement][download][url] = "https://github.com/johndyer/mediaelement/zipball/2.1.9"
;libraries[mediaelement][directory_name] = "mediaelement"
;libraries[mediaelement][destination] = "libraries"
